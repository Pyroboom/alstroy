<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Picture;

class PicturesController extends Controller
{
    //
    public function index() {
        return view('pages.gallery');
    }
}
