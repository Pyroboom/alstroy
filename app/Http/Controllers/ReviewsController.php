<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;

class ReviewsController extends Controller
{
    /*
     * ReviewController
       /reviews      GET:  все отзывы
		             POST: добавить новый.
       /reviews/{id}
                     Не нужны? Рендерим все равно пачкой на странице.
                     PATCH: Править из админки. (публикация?)
                     DELETE: Удалить
     * */
    public function index() {
        //Select only published reviews.
        $reviews = Review::where('published', 1)->get();
        return view('pages.reviews', compact('reviews'));
    }
    public function adminIndex() {
        return view('pages.adminreview');
    }

    public function vueReviews() {
        return $reviews = Review::all()->toArray();
    }
    public function store() {
        Review::create([
           'author' => request('author'),
           'body' => request('body'),
           'rating' => request('rating')
        ]);
        return redirect('/reviews');
    }
    public function update() {
        $review = Review::findOrFail(request('id'));
        $review->body = request('body');
        $review->rating = request('rating');
        $review->published = request('published');

        $comment = request('comment');
        if ($comment == '') {
            $comment = ' ';
        }

        $review->comment = $comment;
        $review->save();
    }
    public function destroy() {
        $review = Review::findOrFail(request('id'));
        $review->delete($review->all());
    }
}
