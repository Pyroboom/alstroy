<?php

namespace App\Http\Controllers;

use App\Mail\IncomingOrders;
use App\Order;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;




class OrdersController extends Controller
{
    //
    public function store() {
            $order = new Order;
            $order->name = request('name');
            $order->email = request('email');
            $order->telephone = request('telephone');
            $order->category = request('category');
            $order->repairtype = request('repairType');
            $order->workamount = request('workAmount');
            $order->save();


            Mail::to('alstroy.spb@gmail.com')->send(new IncomingOrders($order));

            return redirect('/');
    }
}
