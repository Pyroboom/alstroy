<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    //
    protected $dates = ['created_at', 'updated_at'];

    protected $guarded = [];
}
