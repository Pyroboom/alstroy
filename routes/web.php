<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;
use App\Price;

Route::get('/', function () {
    return view('pages.home');
});

Route::get('/contacts', 'Controller@contacts');

Route::get('/gallery', function() {
    return view('pages.gallery');
});

Route::get('/calc', 'PricesController@index');

Route::get('/admin', function() {
    return view('pages.admin');
});


Route::get('/admin/pricelist', 'PricesController@adminIndex');
Route::post('/admin/pricelist', 'PricesController@update');
Route::post('admin/pricelist/add', 'PricesController@store');
Route::post('admin/pricelist/delete/{id}', 'PricesController@destroy');
Route::get('prices/all', 'PricesController@vuePrices');

Route::get('/reviews', 'ReviewsController@index');
Route::post('/reviews', 'ReviewsController@store');
Route::patch('/reviews', 'ReviewsController@update');
Route::post('/reviews/delete', 'ReviewsController@destroy');

Route::get('/admin/reviews', 'ReviewsController@adminIndex');
Route::get('admin/reviews/all', 'ReviewsController@vueReviews');

Route::post('/orders/add', 'OrdersController@store');


