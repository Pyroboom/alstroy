    <div class="col-sm-3 col-md-3">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Admin page</a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <a href="{{url('/admin/pricelist')}}">Прайслист</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="{{'/admin/projects'}}">Проекты</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="{{'/admin/reviews'}}">Отзывы</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>