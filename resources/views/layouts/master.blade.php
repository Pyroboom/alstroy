<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.metadata')
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>

<body>

    <div class="brand">Ал-Строй</div>
    <div class="address-bar">888-88-88 | АдресАдресАдресАдрес |</div>
    <!-- Navigation -->
    @include('layouts.navbar')

    @yield('content')

    <!-- jQuery -->
    <script src="/js/bootstrap/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="/js/bootstrap/bootstrap.min.js"></script>
    <!-- Script to Activate the Carousel -->
    <script src="/js/app.js"></script>
</body>

</html>
