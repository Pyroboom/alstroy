@extends('layouts.master')
@section('content')
    <div id="app" class="container">
        <calcpricelist></calcpricelist>
    </div>
    <script>
        window.prices = {!! $prices->toJson() !!}
    </script>
@endsection

