@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="well">
            <div class="row">
                <div class="col-md-12">
                    <hr>
                    <h2 class="intro-text text-center">Тут - отзывы счастливых покупателей!</h2>
                    <hr>
                </div>
            </div>
        </div>
            @foreach ($reviews as $review)
            <div class="well">
                <div class="row align-items-center">
                    <div class="col-md-3">
                        <h3>{{$review->author}}</h3>
                        <p>{{$review->created_at->toFormattedDateString()}}</p>
                    </div>
                    <div class="col-md-8">
                        @for ($i = 1; $i <= $review->rating; $i++)
                            <label class="rating-star"></label>
                        @endfor
                        <hr>
                        <p>{{$review->body}}</p>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <h3 class="intro-text"><strong>Комментарий работника:</strong></h3>
                        <p>{{$review->comment}}</p>
                    </div>
                </div>

            </div>
            @endforeach

        <div class="well">
            <div class="row">
                <div class="col-md-12">
                    <h3>Оставь свой отзыв:</h3>
                    <form role="form" method="POST" action="/reviews">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="form-group col-lg-6">
                                <label>Имя:</label>
                                <input type="text" class="form-control" name="author">
                            </div>
                            <div class="form-group col-lg-6">
                                <label>Оценка:</label>
                                <select class="form-control" name="rating" id="rating0">
                                    <option value="0">Оценить:</option>
                                    <option value="1">1 - Очень плохо</option>
                                    <option value="2">2 - Плохо</option>
                                    <option value="3">3 - Средне</option>
                                    <option value="4">4 - Хорошо</option>
                                    <option value="5">5 - Отлично</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-12">
                                <label>Отзыв:</label>
                                <textarea class="form-control" rows="6" name="body"></textarea>
                            </div>
                            <div class="form-group col-lg-12">
                                <button type="submit" class="btn btn-default">Отправить</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection