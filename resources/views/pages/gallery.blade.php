@extends ('layouts.master')
@section('content')
    <div class="container">
    <div class="row">
        <div class="box">
            <div class="col-md-4">
                <label>
                    <img class="img-responsive" src="img/slide-1.jpg" />
                </label>
                <hr>
                    <h2 class="intro-text text-center">Ремонт квартир
                    </h2>
                <hr>
            </div>
            <div class="col-md-4">
                <label>
                    <img class="img-responsive" src="img/slide-2.jpg" />
                </label>
                <hr>
                    <h2 class="intro-text text-center">Ремонт домов
                    </h2>
                <hr>
            </div>
            <div class="col-md-4">
                <label>
                    <img class="img-responsive" src="img/slide-3.jpg" />
                </label>
                <hr>
                    <h2 class="intro-text text-center">Ремонт инструмента
                    </h2>
                <hr>
            </div>
        </div>
    </div>
    </div>
    @endsection